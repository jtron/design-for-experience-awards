		
	<div class="wrapper">
		<h2>The International<br /> Design for Experience<br /> Awards</h2>
	
		<p class="description" itemprop="description">We are proud to announce the international Design for Experience (DfE) Awards program. 
		   This program, operated in partnership with <a href="http://uxmag.com" target="_blank">UX Magazine</a>, recognizes people, teams, and companies that 
		   have achieved success in the design of user- and customer-centered products or services.</p>
	   
		<p class="description"><strong>The application window has closed and the judging process has begun.</strong> Because of the deadline extension, the timeline for the notification of semi-finalists and the choosing of winners has been pushed back. If you applied for an award and have questions, please email us at <a href="mailto:awardapps@designforexperience.com">awardapps@designforexperience.com</a></p>
		
		<a href="#apply" target="_blank" class="badge green">Application Deadline Extended to January 19</a>
	</div>